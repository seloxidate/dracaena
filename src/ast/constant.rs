use std::fmt::Display;
use crate::visitor::visitable::*;
use crate::ast::qualifier::Qualifier;

#[derive(Debug, PartialEq, Clone)]
pub enum Constant {
    Integer(i32),
    Double(f64),
    String(String),
    // FIXME: implement date and datetime
    Enum(String, String),
    True,
    False,
    Null,
    Integer64(i64),
    TableMap(Qualifier, String, String)
}

impl Display for Constant {
    
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Integer(int32) => write!(f, "{}", int32),
            Self::Integer64(int64) => write!(f, "{}u", int64),
            Self::Double(float64) => write!(f, "{}", float64),
            Self::Enum(enum_type, value) => write!(f, "{}::{}", enum_type, value),
            Self::String(string) => write!(f, "'{}'", string),
            Self::True => write!(f, "true"),
            Self::False => write!(f, "false"),
            Self::Null => write!(f, "null"),
            Self::TableMap(quali, map, table) => todo!()
        }
    }
}

impl<'a> Visitable<'a> for &'a Constant {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_constant(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_constant(self) {
            visitor.visit_constant(self);
        }
        visitor.leave_visit_constant(self)
    }
}