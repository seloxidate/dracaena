use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::Visitable;
use crate::ast::intrinsics::Intrinsics;
use crate::ast::function::Function;
use crate::ast::eval::Eval;
use crate::ast::assignment::AssignmentStmt;

#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Expression(ExpressionStatement),
}

impl From<AssignmentStmt> for Statement {
    fn from(assign_stmt: AssignmentStmt) -> Self {
        Statement::Expression(ExpressionStatement::AssignmentStmt(assign_stmt))
    }
}

impl From<Eval> for Statement {
    fn from(eval: Eval) -> Self {
        Statement::Expression(
            ExpressionStatement::Eval(eval)
        )
    }
}

impl From<Function> for Statement {
    fn from(func: Function) -> Self {
        Statement::Expression(
            ExpressionStatement::Function(func)
        )
    }
}

impl From<Intrinsics> for Statement {
    fn from(intrinsics: Intrinsics) -> Self {
        Statement::Expression(
            ExpressionStatement::Intrinsics(intrinsics)
        )
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum ExpressionStatement {
    AssignmentStmt(AssignmentStmt),
    Eval(Eval),
    Function(Function),
    Intrinsics(Intrinsics)
}

impl<'a> Visitable<'a> for &'a Statement {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_stmt(self);
        match self {
            Statement::Expression(expr_stmt) => expr_stmt.take_visitor(visitor),
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_stmt(self) {
            visitor.visit_stmt(self);
            continue_leaving = match self {
                Statement::Expression(expr_stmt) => expr_stmt.take_visitor_enter_leave(visitor),
            }
        }
        continue_leaving && visitor.leave_visit_stmt(self)
    }
}

impl<'a> Visitable<'a> for &'a ExpressionStatement {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_expr_stmt(self);
        match self {
            ExpressionStatement::AssignmentStmt(assign_stmt) => assign_stmt.take_visitor(visitor),
            ExpressionStatement::Eval(eval) => eval.take_visitor(visitor),
            ExpressionStatement::Function(func) => func.take_visitor(visitor),
            ExpressionStatement::Intrinsics(intrinsic) => intrinsic.take_visitor(visitor),
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_expr_stmt(self) {
            visitor.visit_expr_stmt(self);
            continue_leaving = match self {
                ExpressionStatement::AssignmentStmt(assign_stmt) => assign_stmt.take_visitor_enter_leave(visitor),
                ExpressionStatement::Eval(eval) => eval.take_visitor_enter_leave(visitor),
                ExpressionStatement::Function(func) => func.take_visitor_enter_leave(visitor),
                ExpressionStatement::Intrinsics(intrinsic) => intrinsic.take_visitor_enter_leave(visitor),
            }
        }
        continue_leaving && visitor.leave_visit_expr_stmt(self)
    }
}