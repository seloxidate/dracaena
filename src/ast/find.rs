use crate::ast::expression::TernaryExpression;
use crate::ast::join::JoinList;
use crate::ast::order_group::OrderGroupData;
use crate::ast::index_hint::IndexHint;
use crate::ast::select_option::SelectOpts;
use crate::ast::table::Table;
use crate::ast::table::TableName;
use crate::visitor::visitable::*;

#[derive(PartialEq, Debug, Clone)]
pub enum FindTable {
    Select(SelectStmt),
    Delete(SelectStmt)
}

impl TableName for FindTable {
    fn table_name(&self) -> &str {
        match self {
            FindTable::Select(select_stmt) | FindTable::Delete(select_stmt) =>
                select_stmt.table_name()
        }
    }
}

impl FindTable {
    pub fn select_stmt(&self) -> &SelectStmt {
        match self {
            Self::Select(stmt) | Self::Delete(stmt) => stmt
        }
    }
}

impl<'a> Visitable<'a> for &'a FindTable {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_find_table(self);
        match self {
            FindTable::Select(select) => select.take_visitor(visitor),
            FindTable::Delete(select) => select.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_find_table(self) {
            visitor.visit_find_table(self);
            continue_leaving = match self {
                FindTable::Select(select) => select.take_visitor_enter_leave(visitor),
                FindTable::Delete(select) => select.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_find_table(self)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum SelectTable {
    SimpleSelect(FindTable),
    WhileSelect(SelectStmt)
}

impl TableName for SelectTable {
    fn table_name(&self) -> &str {
        match self {
            SelectTable::SimpleSelect(find_table) => find_table.table_name(),
            SelectTable::WhileSelect(select_stmt) => select_stmt.table_name()
        }
    }
}

impl<'a> Visitable<'a> for &'a SelectTable {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_select_table(self);
        match self {
            SelectTable::SimpleSelect(find_table) => find_table.take_visitor(visitor),
            SelectTable::WhileSelect(select) => select.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_select_table(self) {
            visitor.visit_select_table(self);
            continue_leaving = match self {
                SelectTable::SimpleSelect(find_table) => find_table.take_visitor_enter_leave(visitor),
                SelectTable::WhileSelect(select) => select.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_select_table(self)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct SelectStmt {
    pub select_opts: Option<Vec<SelectOpts>>,
    pub table: Table
}

impl TableName for SelectStmt {
    fn table_name(&self) -> &str {
        self.table.table_name()
    }
}

impl<'a> Visitable<'a> for &'a SelectStmt {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_select_stmt(self);
        if let Some(select_ops) = &self.select_opts {
            select_ops.iter().for_each(|so| so.take_visitor(visitor));
        }
        &self.table.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_select_stmt(self) {
            visitor.visit_select_stmt(self);
            if let Some(select_ops) = &self.select_opts {
                for so in select_ops.iter() {
                    continue_leaving = so.take_visitor_enter_leave(visitor);
                    if !continue_leaving {
                        break;
                    }
                }
            }
            continue_leaving = continue_leaving && self.table.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_select_stmt(self)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct FindUsing {
    pub select_table: SelectTable,
    pub index_hint: Option<IndexHint>
}

impl TableName for FindUsing {
    fn table_name(&self) -> &str {
        self.select_table.table_name()
    }
}

impl<'a> Visitable<'a> for &'a FindUsing {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_find_using(self);
        &self.select_table.take_visitor(visitor);
        if let Some(index_hint) = &self.index_hint {
            index_hint.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_find_using(self) {
            visitor.visit_find_using(self);
            continue_leaving = self.select_table.take_visitor_enter_leave(visitor);
            if let (Some(index_hint), true) = (&self.index_hint, continue_leaving) {
                continue_leaving = index_hint.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_find_using(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct FindOrder {
    pub find_using: FindUsing,
    pub order_group: Option<OrderGroupData>
}

impl TableName for FindOrder {
    fn table_name(&self) -> &str {
        self.find_using.table_name()
    }
}

impl<'a> Visitable<'a> for &'a FindOrder {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_find_order(self);
        &self.find_using.take_visitor(visitor);
        if let Some(order_group) = &self.order_group {
            order_group.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_find_order(self) {
            visitor.visit_find_order(self);
            continue_leaving = self.find_using.take_visitor_enter_leave(visitor);
            if let (Some(order_group), true) = (&self.order_group, continue_leaving) {
                continue_leaving = order_group.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_find_order(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct FindWhere {
    pub find_order: FindOrder,
    pub if_expr: Option<TernaryExpression>
}

impl TableName for FindWhere {
    fn table_name(&self) -> &str {
        self.find_order.table_name()
    }
}

impl<'a> Visitable<'a> for &'a FindWhere {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_find_where(self);
        &self.find_order.take_visitor(visitor);
        if let Some(if_expr) = &self.if_expr {
            if_expr.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_find_where(self) {
            visitor.visit_find_where(self);
            continue_leaving = self.find_order.take_visitor_enter_leave(visitor);
            if let (Some(if_expr), true) = (&self.if_expr, continue_leaving) {
                continue_leaving = if_expr.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_find_where(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct FindJoin {
    pub find_where: FindWhere,
    pub join_list: Option<JoinList>
}

impl FindJoin {
    pub fn is_while_select(&self) -> bool {
        return match self.find_where.find_order.find_using.select_table {
            SelectTable::WhileSelect(_) => true,
            _ => false
        };
    }
}

impl<'a> Visitable<'a> for &'a FindJoin {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_find_join(self);
        &self.find_where.take_visitor(visitor);
        if let Some(join_list) = &self.join_list {
            join_list.iter().for_each(|js| js.take_visitor(visitor));
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_find_join(self) {
            visitor.visit_find_join(self);
            continue_leaving = self.find_where.take_visitor_enter_leave(visitor);
            if let (Some(join_list), true) = (&self.join_list, continue_leaving) {
                for js in join_list.iter() {
                    continue_leaving = js.take_visitor_enter_leave(visitor);
                    if !continue_leaving {
                        break;
                    }
                }
            }
        }
        continue_leaving && visitor.leave_visit_find_join(self)
    }
}