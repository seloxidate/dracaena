use crate::ast::factor::SecondFactor;
use crate::ast::operator::ComparisonOperator;
use crate::ast::operator::LogicalOperator;
use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::Visitable;
use crate::ast::term::Term;
use crate::ast::factor::{
    ComplementFactor,
    Factor,
    FactorBuilder
};
use crate::ast::operator::AddOperator;
use crate::visitor::visitable::*;
use std::fmt;

#[derive(Debug, PartialEq, Clone)]
pub enum TernaryExpression {
    ConditionalExpression(ConditionalExpression),
    TernaryExpression(IfExpression)
}

impl<'a> Visitable<'a> for &'a TernaryExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_ternary_expr(self);
        match self {
            TernaryExpression::ConditionalExpression(cond) => cond.take_visitor(visitor),
            TernaryExpression::TernaryExpression(if_expr) => if_expr.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_ternary_expr(self) {
            visitor.visit_ternary_expr(self);
            continue_leaving = match self {
                TernaryExpression::ConditionalExpression(cond) => cond.take_visitor_enter_leave(visitor),
                TernaryExpression::TernaryExpression(if_expr) => if_expr.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_ternary_expr(self)
    }
}

impl fmt::Display for TernaryExpression {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            TernaryExpression::ConditionalExpression(cond) => write!(f, "{}", cond)?,
            TernaryExpression::TernaryExpression(if_expr) => {
                write!(f, "{}", if_expr.test_expression)?;
                write!(f, "{}", "\n? ")?;
                write!(f, "{}", if_expr.if_branch)?;
                write!(f, "{}", "\n: ")?;
                write!(f, "{}", if_expr.else_branch)?;
            }
        }
        Ok(())
    }
}

impl TernaryExpression {
    pub fn parenthesize(self) -> TernaryExpression {
        TernaryExpression::ConditionalExpression(
            ConditionalExpression::Expression(
                Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(
                        ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::ParenthesizedTernary(
                                    Box::new(self)
                                )
                            }
                        )
                    )
                ))
            )
        )
    }
}

impl From<FactorBuilder> for TernaryExpression {
    fn from(fb: FactorBuilder) -> Self {
        TernaryExpression::ConditionalExpression(
            fb.into()
        )
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct IfExpression {
    pub test_expression: ConditionalExpression,
    pub if_branch: Box<TernaryExpression>,
    pub else_branch: Box<TernaryExpression>
}

impl<'a> Visitable<'a> for &'a IfExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_if_expr(self);
        &self.test_expression.take_visitor(visitor);
        self.if_branch.take_visitor(visitor);
        self.else_branch.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_if_expr(self) {
            visitor.visit_if_expr(self);
            continue_leaving = self.test_expression.take_visitor_enter_leave(visitor)
                && self.if_branch.take_visitor_enter_leave(visitor)
                && self.else_branch.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_if_expr(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct ConditionalExpressionData {
    pub cond_expr_left: Box<ConditionalExpression>,
    pub log_op: LogicalOperator,
    pub cond_expr_right: Box<ConditionalExpression>
}

impl<'a> Visitable<'a> for &'a ConditionalExpressionData {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_conditional_expr_data(self);
        &self.cond_expr_left.take_visitor(visitor);
        &self.log_op.take_visitor(visitor);
        &self.cond_expr_right.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_conditional_expr_data(self) {
            visitor.visit_conditional_expr_data(self);
            continue_leaving = self.cond_expr_left.take_visitor_enter_leave(visitor)
                && self.log_op.take_visitor_enter_leave(visitor)
                && self.cond_expr_right.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_conditional_expr_data(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum ConditionalExpression {
    ConditionalExpression(ConditionalExpressionData),
    Expression(Expression)
}

impl ConditionalExpression {
    pub fn parenthesized_inner(&self) -> Option<&TernaryExpression> {
        match self {
            ConditionalExpression::Expression(
                Expression::Simple(SimpleExpression::Term(
                    Term::ComplementFactor(
                        ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::ParenthesizedTernary(
                                    inner
                                )
                            }
                        )
                    )
                ))
            ) => Some(&*inner),
            _ => None
        }
    }
}

impl<'a> Visitable<'a> for &'a ConditionalExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_conditional_expr(self);
        match self {
            ConditionalExpression::ConditionalExpression(cond) => cond.take_visitor(visitor),
            ConditionalExpression::Expression(expr) => expr.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_conditional_expr(self) {
            visitor.visit_conditional_expr(self);
            continue_leaving = match self {
                ConditionalExpression::ConditionalExpression(cond) => cond.take_visitor_enter_leave(visitor),
                ConditionalExpression::Expression(expr) => expr.take_visitor_enter_leave(visitor)
            };
        }
        continue_leaving && visitor.leave_visit_conditional_expr(self)
    }
}

/// TODO: this still needs to be implemented
impl fmt::Display for ConditionalExpression {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            _ => unimplemented!(),
        }

        Ok(())
    }
}

impl From<FactorBuilder> for ConditionalExpression {
    fn from(fb: FactorBuilder) -> Self {
        ConditionalExpression::Expression(
            Expression::Simple(
                SimpleExpression::Term(
                    Term::ComplementFactor(
                        if fb.is_true {
                            ComplementFactor::SecondFactor(
                                SecondFactor {
                                    sign_op: fb.sign_op,
                                    factor: fb.factor
                                }
                            )
                        } else {
                            ComplementFactor::NotSecondFactor(
                                SecondFactor {
                                    sign_op: fb.sign_op,
                                    factor: fb.factor
                                }
                            )
                        }
                    )
                )
            )
        )
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Comparison(ComparisonExpression),
    As(AsExpression),
    Is(IsExpression),
    Simple(SimpleExpression)
}

impl<'a> Visitable<'a> for &'a Expression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_expr(self);
        match self {
            Expression::Comparison(comp) => comp.take_visitor(visitor),
            Expression::As(as_ex) => as_ex.take_visitor(visitor),
            Expression::Is(is) => is.take_visitor(visitor),
            Expression::Simple(simple) => simple.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_expr(self) {
            visitor.visit_expr(self);
            continue_leaving = match self {
                Expression::Comparison(comp) => comp.take_visitor_enter_leave(visitor),
                Expression::As(as_ex) => as_ex.take_visitor_enter_leave(visitor),
                Expression::Is(is) => is.take_visitor_enter_leave(visitor),
                Expression::Simple(simple) => simple.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_expr(self)
    }
}

impl Expression {
    pub fn factor(&self) -> Option<&Factor> {
        match self {
            Expression::As(AsExpression {
                simple_expr: SimpleExpression::Term(
                    Term::ComplementFactor(
                        comp_fact
                    )
                ),
                ..
            }) => {
                comp_fact.factor()
            },
            Expression::Is(IsExpression {
                simple_expr: SimpleExpression::Term(
                    Term::ComplementFactor(
                        comp_fact
                    )
                ),
                ..
            }) => {
                comp_fact.factor()
            },
            Expression::Simple(
                SimpleExpression::Term(
                    Term::ComplementFactor(
                        comp_fact
                    )
                )
            ) => {
                comp_fact.factor()
            },
            _ => None
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct ComparisonExpression {
    pub simple_expr_left: SimpleExpression,
    pub comp_op: ComparisonOperator,
    pub simple_expr_right: SimpleExpression
}

impl<'a> Visitable<'a> for &'a ComparisonExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_comparison_expr(self);
        &self.simple_expr_left.take_visitor(visitor);
        &self.comp_op.take_visitor(visitor);
        &self.simple_expr_right.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_comparison_expr(self) {
            visitor.visit_comparison_expr(self);
            continue_leaving = self.simple_expr_left.take_visitor_enter_leave(visitor)
                && self.comp_op.take_visitor_enter_leave(visitor)
                && self.simple_expr_right.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_comparison_expr(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct AsExpression {
    pub simple_expr: SimpleExpression,
    pub id: String
}

impl<'a> Visitable<'a> for &'a AsExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_as_expr(self);
        &self.simple_expr.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_as_expr(self) {
            visitor.visit_as_expr(self);
            continue_leaving = self.simple_expr.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_as_expr(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct IsExpression {
    pub simple_expr: SimpleExpression,
    pub id: String
}

impl<'a> Visitable<'a> for &'a IsExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_is_expr(self);
        &self.simple_expr.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_is_expr(self) {
            visitor.visit_is_expr(self);
            continue_leaving = self.simple_expr.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_is_expr(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum SimpleExpression {
    SimpleExpression(SimpleExpressionData),
    Term(Term)
}

impl<'a> Visitable<'a> for &'a SimpleExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_simple_expr(self);
        match self {
            SimpleExpression::SimpleExpression(simple) => simple.take_visitor(visitor),
            SimpleExpression::Term(term) => term.take_visitor(visitor)
        };
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_simple_expr(self) {
            visitor.visit_simple_expr(self);
            continue_leaving = match self {
                SimpleExpression::SimpleExpression(simple) => simple.take_visitor_enter_leave(visitor),
                SimpleExpression::Term(term) => term.take_visitor_enter_leave(visitor)
            };
        }
        continue_leaving && visitor.leave_visit_simple_expr(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SimpleExpressionData {
    pub simple_expr: Box<SimpleExpression>,
    pub add_op: AddOperator,
    pub term: Term
}

impl<'a> Visitable<'a> for &'a SimpleExpressionData {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_simple_expr_data(self);
        &self.simple_expr.take_visitor(visitor);
        &self.term.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_simple_expr_data(self) {
            visitor.visit_simple_expr_data(self);
            continue_leaving = self.simple_expr.take_visitor_enter_leave(visitor)
                && self.term.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_simple_expr_data(self)
    }
}