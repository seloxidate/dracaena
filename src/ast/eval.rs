use crate::visitor::visitable::*;
use crate::ast::parm::ParmElem;
use crate::ast::eval_name::EvalName;

#[derive(Debug, PartialEq, Clone)]
pub struct Eval {
    pub eval_name: EvalName,
    pub parm_list: Option<Vec<ParmElem>>,
}

impl<'a> Visitable<'a> for &'a Eval {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_eval(self);
        &self.eval_name.take_visitor(visitor);
        if let Some(parm_list) = &self.parm_list {
            parm_list.iter().for_each(|parm_elem| {
                parm_elem.take_visitor(visitor);
            });
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_eval(self) {
            visitor.visit_eval(self);
            continue_leaving = self.eval_name.take_visitor_enter_leave(visitor);
            if let (Some(parm_list), true) = (&self.parm_list, continue_leaving) {
                for parm_elem in parm_list.iter() {
                    continue_leaving = parm_elem.take_visitor_enter_leave(visitor);
                    if !continue_leaving {
                        break;
                    }
                }
            }
        }
        continue_leaving && visitor.leave_visit_eval(self)
    }
}