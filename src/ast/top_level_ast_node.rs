use crate::ast::{
    find::FindJoin,
    local_body::LocalBody,
};

pub enum TopLevelAstNode {
    SelectStmt(FindJoin),
    LocalBody(LocalBody),
}