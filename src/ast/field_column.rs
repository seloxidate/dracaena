use crate::ast::column::Column;
use crate::visitor::visitable::*;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum FieldList {
    All,
    Col(Column),
}

impl<'a> Visitable<'a> for &'a FieldList {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_field_list(self);
        match self {
            FieldList::All => {}, // nothing to do here
            FieldList::Col(col) => col.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_field_list(self) {
            visitor.visit_field_list(self);
            continue_leaving = match self {
                FieldList::All => { true }, // nothing to do here
                FieldList::Col(col) => col.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_field_list(self)
    }
}