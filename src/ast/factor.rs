use crate::ast::expression::TernaryExpression;
use crate::visitor::visitable::*;
use crate::ast::operator::SignOperator;
use crate::ast::eval::Eval;
use crate::ast::intrinsics::Intrinsics;
use crate::ast::function::Function;
use crate::ast::field::Field;
use crate::ast::constant::Constant;
use crate::ast::qualifier::Qualifier;

#[derive(Debug, PartialEq, Clone)]
pub enum ComplementFactor {
    NotSecondFactor(SecondFactor),
    SecondFactor(SecondFactor)
}

impl<'a> Visitable<'a> for &'a ComplementFactor {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_complement_factor(self);
        match self {
            ComplementFactor::NotSecondFactor(sf)
                | ComplementFactor::SecondFactor(sf) => sf.take_visitor(visitor)
        };
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_complement_factor(self) {
            visitor.visit_complement_factor(self);
            continue_leaving = match self {
                ComplementFactor::NotSecondFactor(sf)
                    | ComplementFactor::SecondFactor(sf) => sf.take_visitor_enter_leave(visitor)
            };
        }
        continue_leaving && visitor.leave_visit_complement_factor(self)
    }
}

impl ComplementFactor {
    pub fn factor(&self) -> Option<&Factor> {
        match self {
            ComplementFactor::NotSecondFactor(sf) | ComplementFactor::SecondFactor(sf) => {
                sf.factor()
            },
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SecondFactor {
    pub sign_op: Option<SignOperator>,
    pub factor: Factor
}

impl<'a> Visitable<'a> for &'a SecondFactor {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_second_factor(self);
        &self.factor.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_second_factor(self) {
            visitor.visit_second_factor(self);
            continue_leaving = self.factor.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_second_factor(self)
    }
}

impl SecondFactor {
    pub fn factor(&self) -> Option<&Factor> {
        Some(&self.factor)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Factor {
    ParenthesizedTernary(Box<TernaryExpression>),
    Constant(Constant),
    Field(Field),
    Function(Function),
    Intrinsics(Intrinsics),
    Eval(Eval)
}

type TableAndField<'a> = (&'a str, &'a str);

impl Factor {
    pub fn builder(self) -> FactorBuilder {
        FactorBuilder::new(self)
    }

    pub fn table_and_field(&self) -> Option<TableAndField> {
        match self {
            Self::Field(
                Field::QualifierId(
                    Qualifier::Id(table), field
                )
            ) => {
                Some((table.as_str(), field.as_str()))
            },
            _ => {
                None
            }
        }
    }
}

pub struct FactorBuilder {
    pub(crate) factor: Factor,
    pub(crate) sign_op: Option<SignOperator>,
    pub(crate) is_true: bool,
}

impl FactorBuilder {

    pub fn new(factor: Factor) -> Self {
        Self {
            factor,
            sign_op: None,
            is_true: true
        }
    }

    pub fn sign_op(mut self, sign_op: SignOperator) -> Self {
        self.sign_op = Some(sign_op);
        self
    }

    pub fn not(mut self) -> Self {
        self.is_true = false;
        self
    }
}

impl<'a> Visitable<'a> for &'a Factor {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_factor(self);
        match self {
            Factor::ParenthesizedTernary(tern) => tern.take_visitor(visitor),
            Factor::Constant(constant) => constant.take_visitor(visitor),
            Factor::Field(field) => field.take_visitor(visitor),
            Factor::Function(func) => func.take_visitor(visitor),
            Factor::Intrinsics(int) => int.take_visitor(visitor),
            Factor::Eval(eval) => eval.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_factor(self) {
            visitor.visit_factor(self);
            continue_leaving = match self {
                Factor::ParenthesizedTernary(tern) => tern.take_visitor_enter_leave(visitor),
                Factor::Constant(constant) => constant.take_visitor_enter_leave(visitor),
                Factor::Field(field) => field.take_visitor_enter_leave(visitor),
                Factor::Function(func) => func.take_visitor_enter_leave(visitor),
                Factor::Intrinsics(int) => int.take_visitor_enter_leave(visitor),
                Factor::Eval(eval) => eval.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_factor(self)
    }
}