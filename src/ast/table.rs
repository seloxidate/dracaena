use crate::ast::common::TableIdentifier;
use crate::visitor::visitable::*;
use crate::ast::from::From;
use crate::ast::field_column::FieldList;

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum Table {
    Standalone(TableIdentifier),
    WithFields(TableWithFields)
}

pub trait TableName {
    fn table_name(&self) -> &str;
}

impl TableName for Table {
    fn table_name(&self) -> &str {
        match self {
            Table::Standalone(id) => id,
            Table::WithFields(twf) => twf.from.table.as_str()
        }
    }
}

impl FieldListable for Table {
    fn field_list(&self) -> Option<&[FieldList]> {
        match self {
            Self::Standalone(_) => None,
            Self::WithFields(table_with_fields) => table_with_fields.field_list()
        }
    }
}

impl<'a> Visitable<'a> for &'a Table {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_table(self);
        match self {
            Table::Standalone(id) => {},
            Table::WithFields(twf) => twf.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_table(self) {
            visitor.visit_table(self);
            continue_leaving = match self {
                Table::Standalone(id) => { true },
                Table::WithFields(twf) => twf.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_table(self)
    }
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct TableWithFields {
    pub fields: Vec<FieldList>,
    pub from: From
}

pub trait FieldListable {
    fn field_list(&self) -> Option<&[FieldList]>;
}

impl FieldListable for TableWithFields {
    fn field_list(&self) -> Option<&[FieldList]> {
        Some(&self.fields)
    }
}

impl<'a> Visitable<'a> for &'a TableWithFields {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_table_with_fields(self);
        &self.from.take_visitor(visitor);
        &self.fields.iter().for_each(|field| field.take_visitor(visitor));
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_table_with_fields(self) {
            visitor.visit_table_with_fields(self);
            continue_leaving = self.from.take_visitor_enter_leave(visitor);
            if continue_leaving {
                for field in &self.fields {
                    continue_leaving = field.take_visitor_enter_leave(visitor);
                    if !continue_leaving {
                        break;
                    }
                }
            }
        }
        continue_leaving && visitor.leave_visit_table_with_fields(self)
    }
}