use crate::visitor::visitable::*;
use crate::ast::qualifier::Qualifier;

#[derive(Debug, PartialEq, Clone)]
pub enum EvalName {
    Id(String),
    IdDblColonId(String, String),
    Super,
    New(String),
    Qualifier(Qualifier, String),
    QualifierDblColon(Qualifier, String, String)
}

impl<'a> Visitable<'a> for &'a EvalName {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_eval_name(self)
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_eval_name(self) {
            visitor.visit_eval_name(self);
            continue_leaving = match self {
                EvalName::Qualifier(quali, _) | EvalName::QualifierDblColon(quali, _, _) => quali.take_visitor_enter_leave(visitor),
                _ => { true }
            };
        }
        continue_leaving && visitor.leave_visit_eval_name(self)
    }
}