use crate::ast::common::FieldIdentifier;
use crate::visitor::visitable::*;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct IndexHint {
    pub hint: bool,
    pub id: FieldIdentifier
}

impl<'a> Visitable<'a> for &'a IndexHint {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_index_hint(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_index_hint(self) {
            visitor.visit_index_hint(self);
        }
        visitor.leave_visit_index_hint(self)
    }
}