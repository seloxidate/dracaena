use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::Visitable;
use crate::ast::statement::Statement;
use crate::ast::declaration::DeclarationStmt;

#[derive(Debug, PartialEq, Clone, Default)]
pub struct LocalBody {
    pub dcl_list: Vec<DeclarationStmt>,
    pub stmt_list: Vec<Statement>
}

impl<'a> Visitable<'a> for &'a LocalBody {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_local_body(self);
        self.dcl_list.iter().for_each(|stmt| stmt.take_visitor(visitor));
        self.stmt_list.iter().for_each(|stmt| stmt.take_visitor(visitor));
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_local_body(self) {
            visitor.visit_local_body(self);
            continue_leaving = self.dcl_list.iter().all(|dcl| dcl.take_visitor_enter_leave(visitor))
                && self.stmt_list.iter().all(|stmt| stmt.take_visitor_enter_leave(visitor));
        }
        continue_leaving && visitor.leave_visit_local_body(self)
    }
}