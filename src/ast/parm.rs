use crate::visitor::visitable::*;
use crate::ast::expression::TernaryExpression;

#[derive(Debug, PartialEq, Clone)]
pub enum ParmElem {
    TernaryExpression(Box<TernaryExpression>),
    // FIXME: ByRef(Field) we don't need that, because it is only for .NET interop
}

impl<'a> Visitable<'a> for &'a ParmElem {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
       visitor.visit_parm_elem(self);
       match self {
           ParmElem::TernaryExpression(tern_expr) => tern_expr.take_visitor(visitor)
       }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_parm_elem(self) {
            visitor.visit_parm_elem(self);
            continue_leaving = match self {
                ParmElem::TernaryExpression(tern_expr) => tern_expr.take_visitor_enter_leave(visitor)
            };
        }
        continue_leaving && visitor.leave_visit_parm_elem(self)
    }
}