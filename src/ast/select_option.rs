use crate::visitor::visitable::*;

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub enum FirstOnly {
    FirstOnly,
    FirstOnly1,
    FirstOnly10,
    FirstOnly100,
    FirstOnly1000,
}

impl<'a> Visitable<'a> for &'a FirstOnly {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_first_only(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_first_only(self) {
            visitor.visit_first_only(self);
        }
        visitor.leave_visit_first_only(self)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub enum ForcePlaceholderLiteral {
    ForceLiterals,
    ForcePlaceholders,
}

impl<'a> Visitable<'a> for &'a ForcePlaceholderLiteral {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_force_placeholder_literal(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_force_placeholder_literal(self) {
            visitor.visit_force_placeholder_literal(self);
        }
        visitor.leave_visit_force_placeholder_literal(self)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, PartialOrd, Ord)]
pub enum SelectOpts {
    Reverse,
    FirstFast,
    FirstOnly(FirstOnly),
    ForUpdate,
    NoFetch,
    ForceSelectOrder,
    ForcePlaceholderLiteral(ForcePlaceholderLiteral),
    ForceNestedLoop,
    RepeatableRead,
    ValidTimeState,
}

impl<'a> Visitable<'a> for &'a SelectOpts {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_select_opts(self);
        match self {
            SelectOpts::FirstOnly(fo) => fo.take_visitor(visitor),
            SelectOpts::ForcePlaceholderLiteral(fpl) => fpl.take_visitor(visitor),
            _ => {} // nothing to do here
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_select_opts(self) {
            visitor.visit_select_opts(self);
            continue_leaving = match self {
                SelectOpts::FirstOnly(fo) => fo.take_visitor_enter_leave(visitor),
                SelectOpts::ForcePlaceholderLiteral(fpl) => fpl.take_visitor_enter_leave(visitor),
                _ => true,
            }
        }
        continue_leaving && visitor.leave_visit_select_opts(self)
    }
}
