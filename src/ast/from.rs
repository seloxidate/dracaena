use crate::ast::common::TableIdentifier;
use crate::visitor::visitable::*;

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct From {
    pub table: TableIdentifier
}

impl<'a> Visitable<'a> for &'a From {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_from(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_from(self) {
            visitor.visit_from(self);
        }
        visitor.leave_visit_from(self)
    }
}