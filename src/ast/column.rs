use crate::ast::common::FieldIdentifier;
use crate::ast::common::TableIdentifier;
use crate::visitor::visitable::*;
use std::fmt;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum FunctionExpression {
    Sum(FieldIdentifier),
    Avg(FieldIdentifier),
    Count(FieldIdentifier),
    MinOf(FieldIdentifier),
    MaxOf(FieldIdentifier)
}

impl<'a> Visitable<'a> for &'a FunctionExpression {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_function_expr(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_function_expr(self) {
            visitor.visit_function_expr(self);
        }
        visitor.leave_visit_function_expr(self)
    }
}

impl FunctionExpression {
    pub fn field(&self) -> &FieldIdentifier {
        match self {
            FunctionExpression::Avg(field) => field,
            FunctionExpression::Count(field) => field,
            FunctionExpression::MaxOf(field) => field,
            FunctionExpression::MinOf(field) => field,
            FunctionExpression::Sum(field) => field,
        }
    }
}

impl fmt::Display for FunctionExpression {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            FunctionExpression::Avg(field) => write!(f, "{}({})", "avg", field)?,
            FunctionExpression::Count(field) => write!(f, "{}({})", "count", field)?,
            FunctionExpression::MaxOf(field) => write!(f, "{}({})", "maxOf", field)?,
            FunctionExpression::MinOf(field) => write!(f, "{}({})", "minOf", field)?,
            FunctionExpression::Sum(field) => write!(f, "{}({})", "sum", field)?
        }

        Ok(())
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Column {
    pub name: FieldIdentifier,
    pub function: Option<FunctionExpression>,
    pub index: Option<u32>,
}

impl<'a> Visitable<'a> for &'a Column {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_column(self);
        if let Some(func_expr) = &self.function {
            func_expr.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_column(self) {
            visitor.visit_column(self);
            if let Some(func_expr) = &self.function {
                continue_leaving = func_expr.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_column(self)
    }
}

impl fmt::Display for Column {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.function {
            Some(func) => write!(f, "{}", func)?,
            None => {
                write!(f, "{}", self.name);
                match &self.index {
                    Some(index) => write!(f, "[{}]", index)?,
                    None => {}
                }
            }
        }

        Ok(())
    }
}

impl From<FunctionExpression> for Column {
    fn from(func_expr: FunctionExpression) -> Column {
        Column {
            name: func_expr.field().to_owned(),
            function: Some(func_expr),
            index: None
        }
    }
}