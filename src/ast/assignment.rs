
use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::Visitable;
use crate::ast::expression::TernaryExpression;
use crate::ast::field::Field;

#[derive(Debug, PartialEq, Clone)]
pub struct AssignmentClause {
    pub tern_expr: TernaryExpression
}

#[derive(Debug, PartialEq, Clone)]
pub enum AssignmentStmt {
    FieldAssign {
        field: Field,
        assign: Assign,
        tern_expr: TernaryExpression
    },
    DestructureAssign {
        field_list: Vec<Field>,
        tern_expr: TernaryExpression
    },
    PostIncDec(PrePostIncDecAssign),
    PreIncDec(PrePostIncDecAssign)
}

#[derive(Debug, PartialEq, Clone)]
pub struct PrePostIncDecAssign {
    pub field: Field,
    pub inc_dec: AssignIncDec
}

/// This is basically += and -=
#[derive(Debug, PartialEq, Clone)]
pub enum Assign {
    AssignEqual,
    AssignInc,
    AssignDec
}

/// This is basically ++ and --
#[derive(Debug, PartialEq, Clone)]
pub enum AssignIncDec {
    Inc,
    Dec
}

impl<'a> Visitable<'a> for &'a AssignmentClause {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_assign_clause(self);
        self.tern_expr.take_visitor(visitor)
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_assign_clause(self) {
            visitor.visit_assign_clause(self);
            continue_leaving = self.tern_expr.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_assign_clause(self)
    }
}

impl<'a> Visitable<'a> for &'a AssignmentStmt {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_assign_stmt(self);
        match self {
            AssignmentStmt::FieldAssign {
                field,
                assign,
                tern_expr
            } => {
                field.take_visitor(visitor);
                assign.take_visitor(visitor);
                tern_expr.take_visitor(visitor);
            },
            AssignmentStmt::DestructureAssign {
                field_list,
                tern_expr
            } => {
                field_list.iter().for_each(|field| field.take_visitor(visitor));
                tern_expr.take_visitor(visitor);
            },
            AssignmentStmt::PreIncDec(pre_post_inc_dec_assign)
            | AssignmentStmt::PostIncDec(pre_post_inc_dec_assign) => {
                pre_post_inc_dec_assign.take_visitor(visitor);
            },
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_assign_stmt(self) {
            visitor.visit_assign_stmt(self);
            continue_leaving = match self {
                AssignmentStmt::FieldAssign {
                    field,
                    assign,
                    tern_expr
                } => field.take_visitor_enter_leave(visitor)
                    && assign.take_visitor_enter_leave(visitor)
                    && tern_expr.take_visitor_enter_leave(visitor),
                AssignmentStmt::DestructureAssign {
                    field_list,
                    tern_expr
                } => field_list.iter().all(|field| field.take_visitor_enter_leave(visitor))
                    && tern_expr.take_visitor_enter_leave(visitor),
                AssignmentStmt::PreIncDec(pre_post_inc_dec_assign)
                | AssignmentStmt::PostIncDec(pre_post_inc_dec_assign) => {
                    pre_post_inc_dec_assign.take_visitor_enter_leave(visitor)
                },
            }
        }
        continue_leaving && visitor.leave_visit_assign_stmt(self)
    }
}

impl<'a> Visitable<'a> for &'a PrePostIncDecAssign {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_pre_post_inc_dec_assign(self);
        self.field.take_visitor(visitor);
        self.inc_dec.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_pre_post_inc_dec_assign(self) {
            self.take_visitor(visitor);
            continue_leaving = self.field.take_visitor_enter_leave(visitor)
                && self.inc_dec.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_pre_post_inc_dec_assign(self)
    }
}

impl<'a> Visitable<'a> for &'a Assign {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_assign(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_assign(self) {
            visitor.visit_assign(self);
        }
        visitor.leave_visit_assign(self)
    }
}

impl<'a> Visitable<'a> for &'a AssignIncDec {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_assign_inc_dec(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_assign_inc_dec(self) {
            visitor.visit_assign_inc_dec(self);
        }
        visitor.leave_visit_assign_inc_dec(self)
    }
}