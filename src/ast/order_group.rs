use crate::ast::order_elem::OrderElem;
use crate::visitor::visitable::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum OrderGroup {
    GroupBy(GroupOrderBy),
    OrderBy(GroupOrderBy)
}

impl<'a> Visitable<'a> for &'a OrderGroup {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_order_group(self);
        match self {
            OrderGroup::GroupBy(gob) | OrderGroup::OrderBy(gob) => gob.take_visitor(visitor),
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_order_group(self) {
            visitor.visit_order_group(self);
            continue_leaving = match self {
                OrderGroup::GroupBy(gob) | OrderGroup::OrderBy(gob) => gob.take_visitor_enter_leave(visitor),
            }
        }
        continue_leaving && visitor.leave_visit_order_group(self)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct OrderGroupData {
    pub order_group: OrderGroup,
    pub other_order_group: Option<OrderGroup>
}

impl<'a> Visitable<'a> for &'a OrderGroupData {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_order_group_data(self);
        &self.order_group.take_visitor(visitor);
        if let Some(other_order_group) = &self.other_order_group {
            other_order_group.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_order_group_data(self) {
            visitor.visit_order_group_data(self);
            continue_leaving = self.order_group.take_visitor_enter_leave(visitor);
            if let (Some(other_order_group), true) = (&self.other_order_group, continue_leaving) {
                continue_leaving = other_order_group.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_order_group_data(self)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct GroupOrderBy {
    pub by: bool,
    pub order_elems: Vec<OrderElem>
}

impl<'a> Visitable<'a> for &'a GroupOrderBy {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_group_order_by(self);
        &self.order_elems.iter().for_each(|oe| oe.take_visitor(visitor));
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_group_order_by(self) {
            visitor.visit_group_order_by(self);
            for oe in &self.order_elems {
                continue_leaving = oe.take_visitor_enter_leave(visitor);
                if !continue_leaving {
                    break;
                }
            }
        }
        continue_leaving && visitor.leave_visit_group_order_by(self)
    }
}