use std::error::Error;
use crate::visitor::visitable::*;
use crate::ast::qualifier::Qualifier;
use crate::ast::intrinsics::Intrinsics;
use crate::ast::common::uppercase_first_letter;

// TODO: this is not finished and only covers the basics
#[derive(Debug, PartialEq, Clone)]
pub enum Field {
    QualifierId(Qualifier, String),
    Id(String)
}

impl Field {
    pub fn try_mk_intrinsics_field_num(&self) -> Result<Intrinsics, String> {
        match self {
            Self::QualifierId(
                Qualifier::Id(table),
                field
            ) => {
                Ok(Intrinsics::FieldNum(uppercase_first_letter(table), field.to_owned()))
            },
            _ => Err(format!("Could not make intrinsics fieldNum from {:?}", self))
        }
    }

    pub fn try_mk_intrinsics_field_str(&self) -> Result<Intrinsics, String> {
        self.try_mk_intrinsics_field_num()
            .map(|field_num| {
                match field_num {
                    Intrinsics::FieldNum(table, field) => Intrinsics::FieldStr(table, field),
                    _ => field_num
                }
            })
            .map_err(|_err| format!("Could not make intrinsics fieldStr from {:?}", self))
    }
}

impl<'a> Visitable<'a> for &'a Field {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_field(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_field(self) {
            visitor.visit_field(self);
            continue_leaving = match self {
                Field::QualifierId(quali, _) => quali.take_visitor_enter_leave(visitor),
                _ => { true }
            }
        }
        continue_leaving && visitor.leave_visit_field(self)
    }
}