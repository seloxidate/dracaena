use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::Visitable;
use crate::ast::eval::Eval;

#[derive(Debug, PartialEq, Clone)]
pub enum Qualifier {
    Eval(Box<Eval>),
    Id(String)
}

impl<'a> Visitable<'a> for &'a Qualifier {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_qualifier(self)
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_qualifier(self) {
            visitor.visit_qualifier(self);
            continue_leaving = match self {
                Qualifier::Eval(eval) => eval.take_visitor_enter_leave(visitor),
                _ => { true }
            }
        }
        continue_leaving && visitor.leave_visit_qualifier(self)
    }
}