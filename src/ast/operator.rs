use std::fmt::Display;
use crate::visitor::visitable::*;


#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum ComparisonOperator {
    Equal,
    NotEqual,
    GreaterEqual,
    LessEqual,
    Greater,
    Less,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum LogicalOperator {
    Or,
    And,
}

impl Display for LogicalOperator {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self {
            Self::And => write!(f, "{}", "&&"),
            Self::Or => write!(f, "{}", "||")
        }
    }
}

impl<'a> Visitable<'a> for &'a LogicalOperator {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_logical_op(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_logical_op(self) {
            visitor.visit_logical_op(self);
        }
        visitor.leave_visit_logical_op(self)
    }
}

impl<'a> Visitable<'a> for &'a ComparisonOperator {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_comparison_op(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_comparison_op(self) {
            visitor.visit_comparison_op(self);
        }
        visitor.leave_visit_comparison_op(self)
    }
}

// TODO: we haven't implemented visitors for the other operators yet

#[derive(Debug, PartialEq, Clone)]
pub enum AddOperator {
    Plus,
    Minus,
    PhysicalOr
}

#[derive(Debug, PartialEq, Clone)]
pub enum MultiplyOperator {
    Mult,
    Div,
    Mod,
    DivInt,
    ShiftLeft,
    ShiftRight,
    PhysicalAnd,
    PhysicalXOr
}

#[derive(Debug, PartialEq, Clone)]
pub enum SignOperator {
    Minus,
    PhysicalNot
}