use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitable;
use crate::visitor::visitable::Visitor;
use crate::ast::assignment::AssignmentClause;

#[derive(Debug, PartialEq, Clone)]
pub struct DeclarationStmt {
    pub decl_init_list: DeclarationInitList
}

#[derive(Debug, PartialEq, Clone)]
pub enum DeclarationInitList {
    DeclarationInit(DeclarationInit),
    DeclarationCommaList(DeclarationCommaList)
}

#[derive(Debug, PartialEq, Clone)]
pub struct DeclarationInit {
    pub declaration: Declaration,
    pub assignment_clause: Option<AssignmentClause>
}

#[derive(Debug, PartialEq, Clone)]
pub struct DeclarationCommaList {
    pub declaration: Declaration,
    pub id_with_assignment_clauses: Vec<IdWithAssigmentClause>
}

#[derive(Debug, PartialEq, Clone)]
pub struct IdWithAssigmentClause {
    pub id: String,
    pub assignment_clause: Option<AssignmentClause>
}

#[derive(Debug, PartialEq, Clone)]
pub struct Declaration {
    pub decl_type: DeclarationType,
    pub id: String
}

#[derive(Debug, PartialEq, Clone)]
pub enum DeclarationType {
    // there are a lot more types, but at the moment we only need one
    TypeId(String)
}

impl<'a> Visitable<'a> for &'a DeclarationStmt {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_dcl_stmt(self);
        self.decl_init_list.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_dcl_stmt(self) {
            visitor.visit_dcl_stmt(self);
            continue_leaving = self.decl_init_list.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_dcl_stmt(self)
    }
}

impl<'a> Visitable<'a> for &'a DeclarationInitList {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_dcl_init_list(self);
        match self {
            DeclarationInitList::DeclarationInit(dcl_init) => dcl_init.take_visitor(visitor),
            DeclarationInitList::DeclarationCommaList(dcl_comma_list) => dcl_comma_list.take_visitor(visitor),
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_dcl_init_list(self) {
            visitor.visit_dcl_init_list(self);
            continue_leaving = match self {
                DeclarationInitList::DeclarationInit(dcl_init) => dcl_init.take_visitor_enter_leave(visitor),
                DeclarationInitList::DeclarationCommaList(dcl_comma_list) => dcl_comma_list.take_visitor_enter_leave(visitor),
            }
        }
        continue_leaving && visitor.leave_visit_dcl_init_list(self)
    }
}

impl<'a> Visitable<'a> for &'a DeclarationInit {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_dcl_init(self);
        self.declaration.take_visitor(visitor);
        if let Some(assign) = &self.assignment_clause {
            assign.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_dcl_init(self) {
            visitor.visit_dcl_init(self);
            continue_leaving = self.declaration.take_visitor_enter_leave(visitor)
                && self.assignment_clause.as_ref().map_or(true, |assign| assign.take_visitor_enter_leave(visitor));
        }
        continue_leaving && visitor.leave_visit_dcl_init(self)
    }
}

impl<'a> Visitable<'a> for &'a DeclarationCommaList {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_dcl_comma_list(self);
        self.declaration.take_visitor(visitor);
        self.id_with_assignment_clauses.iter().for_each(|id_assign| id_assign.take_visitor(visitor));
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_dcl_comma_list(self) {
            visitor.visit_dcl_comma_list(self);
            continue_leaving = self.declaration.take_visitor_enter_leave(visitor)
                && self.id_with_assignment_clauses.iter().all(|id_assign| id_assign.take_visitor_enter_leave(visitor));
        }
        continue_leaving && visitor.leave_visit_dcl_comma_list(self)
    }
}

impl<'a> Visitable<'a> for &'a IdWithAssigmentClause {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_id_with_assign_clause(self);
        if let Some(assign_clause) = &self.assignment_clause {
            assign_clause.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_id_with_assign_clause(self) {
            visitor.visit_id_with_assign_clause(self);
            continue_leaving = self.assignment_clause.as_ref().map_or(true, |assign_clause| assign_clause.take_visitor_enter_leave(visitor));
        }
        continue_leaving && visitor.leave_visit_id_with_assign_clause(self)
    }
}

impl<'a> Visitable<'a> for &'a Declaration {
    
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_dcl(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_dcl(self) {
            visitor.visit_dcl(self);
        }
        visitor.leave_visit_dcl(self)
    }
}