use crate::visitor::visitable::*;
use crate::ast::parm::ParmElem;

/// see https://docs.microsoft.com/en-us/dynamicsax-2012/developer/x-function-categories-and-their-functions
/// we only use a subset
#[derive(Debug, PartialEq, Clone)]
pub enum Function {
    StrFmt(Vec<ParmElem>),
    Any2Int(ParmElem),
    // this one is not in the official docs, but it absolutely makes sense to have it here
    QueryValue(ParmElem)
}

impl<'a> Visitable<'a> for &'a Function {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_function(self);
        match self {
            Function::StrFmt(parm_elems) => parm_elems.iter().for_each(|pe| {
                pe.take_visitor(visitor);
            }),
            Function::Any2Int(parm_elem) | Function::QueryValue(parm_elem) => parm_elem.take_visitor(visitor),
        };
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_function(self) {
            visitor.visit_function(self);
            continue_leaving = match self {
                Function::StrFmt(parm_elems) => {
                    for pe in parm_elems {
                        continue_leaving = pe.take_visitor_enter_leave(visitor);
                        if !continue_leaving {
                            break;
                        }
                    }
                    continue_leaving
                },
                Function::Any2Int(parm_elem) | Function::QueryValue(parm_elem) => parm_elem.take_visitor_enter_leave(visitor)
            };
        }
        continue_leaving && visitor.leave_visit_function(self)
    }
}