use crate::ast::field_column::FieldList;
use crate::ast::table::FieldListable;
use crate::ast::table::TableName;
use crate::ast::order_group::OrderGroupData;
use crate::ast::table::Table;
use crate::ast::select_option::SelectOpts;
use crate::ast::index_hint::IndexHint;
use crate::visitor::visitable::*;
use crate::ast::expression::TernaryExpression;

#[derive(Debug, PartialEq, Clone)]
pub enum JoinVariant {
    Outer,
    Exists,
    NotExists
}

impl<'a> Visitable<'a> for &'a JoinVariant {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_join_variant(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_join_variant(self) {
            visitor.visit_join_variant(self);
        }
        visitor.leave_visit_join_variant(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct JoinClause {
    pub join_variant: Option<JoinVariant>,
    pub select_opts: Option<Vec<SelectOpts>>,
    pub table: Table
}

impl TableName for JoinClause {
    fn table_name(&self) -> &str {
        self.table.table_name()
    }
}

impl FieldListable for JoinClause {
    fn field_list(&self) -> Option<&[FieldList]> {
        self.table.field_list()
    }
}

impl<'a> Visitable<'a> for &'a JoinClause {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_join_clause(self);
        if let Some(select_opts) = &self.select_opts {
            select_opts.iter().for_each(|so| so.take_visitor(visitor));
        }
        &self.table.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_join_clause(self) {
            visitor.visit_join_clause(self);
            if let Some(select_opts) = &self.select_opts {
                for so in select_opts {
                    continue_leaving = so.take_visitor_enter_leave(visitor);
                    if !continue_leaving {
                        break;
                    }
                }
            }
            continue_leaving = continue_leaving && self.table.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_join_clause(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct JoinUsing {
    pub join_clause: JoinClause,
    pub index_hint: Option<IndexHint>
}

impl TableName for JoinUsing {
    fn table_name(&self) -> &str {
        self.join_clause.table_name()
    }
}

impl<'a> Visitable<'a> for &'a JoinUsing {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_join_using(self);
        &self.join_clause.take_visitor(visitor);
        if let Some(index_hint) = &self.index_hint {
            index_hint.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_join_using(self) {
            visitor.visit_join_using(self);
            continue_leaving = self.join_clause.take_visitor_enter_leave(visitor);
            if let (Some(index_hint), true) = (&self.index_hint, continue_leaving) {
                continue_leaving = index_hint.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_join_using(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct JoinOrder {
    pub join_using: JoinUsing,
    pub order_group: Option<OrderGroupData>
}

impl TableName for JoinOrder {
    fn table_name(&self) -> &str {
        self.join_using.table_name()
    }
}

impl<'a> Visitable<'a> for &'a JoinOrder {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_join_order(self);
        &self.join_using.take_visitor(visitor);
        if let Some(order_group) = &self.order_group {
            order_group.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_join_order(self) {
            visitor.visit_join_order(self);
            continue_leaving = self.join_using.take_visitor_enter_leave(visitor);
            if let (Some(order_group), true) = (&self.order_group, continue_leaving) {
                continue_leaving = order_group.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_join_order(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct JoinSpec {
    pub join_order: JoinOrder,
    pub if_expr: Option<TernaryExpression>
}

impl TableName for JoinSpec {
    fn table_name(&self) -> &str {
        self.join_order.table_name()
    }
}

impl<'a> Visitable<'a> for &'a JoinSpec {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_join_spec(self);
        &self.join_order.take_visitor(visitor);
        if let Some(if_expr) = &self.if_expr {
            if_expr.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_join_spec(self) {
            visitor.visit_join_spec(self);
            continue_leaving = self.join_order.take_visitor_enter_leave(visitor);
            if let (Some(if_expr), true) = (&self.if_expr, continue_leaving) {
                continue_leaving = if_expr.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_join_spec(self)
    }
}

pub type JoinList = Vec<JoinSpec>;