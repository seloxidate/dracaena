use std::fmt::Display;
use crate::ast::common::TableIdentifier;
use crate::visitor::visitable::*;

/// see https://docs.microsoft.com/en-us/dynamicsax-2012/developer/intrinsic-functions
/// we only use a subset of the available functions
#[derive(Debug, PartialEq, Clone)]
pub enum Intrinsics {
    FieldNum(String, String),
    FieldStr(String, String),
    TableNum(String)
}

impl Display for Intrinsics {
    
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::FieldNum(table, field) => write!(f, "fieldNum({}, {})", table, field),
            Self::FieldStr(table, field) => write!(f, "fieldStr({}, {})", table, field),
            Self::TableNum(table)        => write!(f, "tableNum({})", table)
        }
    }
}

impl Intrinsics {
    pub fn table_name_and_field_name(&self) -> (&str, Option<&str>) {
        match self {
            Self::FieldNum(table, field) | Self::FieldStr(table, field) => (table, Some(field)),
            Self::TableNum(table) => (table, None)
        }
    }

    pub fn into_table_name_and_field_name(self) -> (String, Option<String>) {
        match self {
            Self::FieldNum(table, field) | Self::FieldStr(table, field) => (table, Some(field)),
            Self::TableNum(table) => (table, None)
        }
    }
}

impl<'a> Visitable<'a> for &'a Intrinsics {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_intrinsics(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_intrinsics(self) {
            visitor.visit_intrinsics(self);
        }
        visitor.leave_visit_intrinsics(self)
    }
}