use crate::ast::column::Column;
use crate::visitor::visitable::*;
use crate::ast::common::*;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Direction {
    Ascending,
    Descending
}

impl<'a> Visitable<'a> for &'a Direction {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_direction(self);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        if visitor.enter_visit_direction(self) {
            visitor.visit_direction(self);
        }
        visitor.leave_visit_direction(self)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct OrderElem {
    pub table: Option<TableIdentifier>,
    pub field: FieldIdentifier,
    pub index: Option<u32>,
    pub direction: Option<Direction>
}

impl<'a> Visitable<'a> for &'a OrderElem {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_order_elem(self);
        if let Some(dir) = &self.direction {
            dir.take_visitor(visitor);
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_order_elem(self) {
            visitor.visit_order_elem(self);
            if let Some(dir) = &self.direction {
                continue_leaving = dir.take_visitor_enter_leave(visitor);
            }
        }
        continue_leaving && visitor.leave_visit_order_elem(self)
    }
}

impl From<&Column> for OrderElem {
    fn from(col: &Column) -> OrderElem {
        OrderElem {
            field: col.name.to_owned(),
            table: None,
            index: col.index,
            direction: None
        }
    }
}