use crate::ast::operator::MultiplyOperator;
use crate::visitor::visitable::Visitor;
use crate::visitor::visitable::VisitorEnterLeave;
use crate::visitor::visitable::Visitable;
use crate::ast::factor::ComplementFactor;
use crate::ast::operator::AddOperator;

#[derive(Debug, PartialEq, Clone)]
pub enum Term {
    Term(TermData),
    ComplementFactor(ComplementFactor)
}

impl<'a> Visitable<'a> for &'a Term {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_term(self);
        match self {
            Term::Term(term_data) => term_data.take_visitor(visitor),
            Term::ComplementFactor(fac) => fac.take_visitor(visitor)
        }
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_term(self) {
            visitor.visit_term(self);
            continue_leaving = match self {
                Term::Term(term_data) => term_data.take_visitor_enter_leave(visitor),
                Term::ComplementFactor(fac) => fac.take_visitor_enter_leave(visitor)
            }
        }
        continue_leaving && visitor.leave_visit_term(self)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct TermData {
    pub term: Box<Term>,
    pub mul_op: MultiplyOperator,
    pub cmpl_fact: ComplementFactor
}

impl<'a> Visitable<'a> for &'a TermData {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T) {
        visitor.visit_term_data(self);
        &self.term.take_visitor(visitor);
        &self.cmpl_fact.take_visitor(visitor);
    }

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool {
        let mut continue_leaving = true;
        if visitor.enter_visit_term_data(self) {
            visitor.visit_term_data(self);
            continue_leaving = self.term.take_visitor_enter_leave(visitor)
                && self.cmpl_fact.take_visitor_enter_leave(visitor);
        }
        continue_leaving && visitor.leave_visit_term_data(self)
    }
}