use crate::ast::declaration::*;
use crate::ast::assignment::*;
use crate::ast::statement::*;
use crate::ast::local_body::LocalBody;
use crate::ast::qualifier::Qualifier;
use crate::ast::operator::ComparisonOperator;
use crate::ast::operator::LogicalOperator;
use crate::ast::eval_name::EvalName;
use crate::ast::eval::Eval;
use crate::ast::intrinsics::Intrinsics;
use crate::ast::parm::ParmElem;
use crate::ast::function::Function;
use crate::ast::field::Field;
use crate::ast::constant::Constant;
use crate::ast::factor::Factor;
use crate::ast::factor::SecondFactor;
use crate::ast::factor::ComplementFactor;
use crate::ast::term::Term;
use crate::ast::term::TermData;
use crate::ast::expression::*;
use crate::ast::expression;
use crate::ast::{column::*,
    field_column::*,
    find::*,
    from::*,
    index_hint::*,
    join::*,
    order_elem::*,
    order_group::*,
    select_option::*,
    table::*,
};

pub trait Visitable<'a> {
    fn take_visitor<T: Visitor<'a>>(self, visitor: &mut T);

    fn take_visitor_enter_leave<T: VisitorEnterLeave<'a>>(self, visitor: &mut T) -> bool;
}

pub trait Visitor<'a> {
    fn visit_dcl_stmt(&mut self, node: &'a DeclarationStmt);

    fn visit_dcl_init_list(&mut self, node: &'a DeclarationInitList);

    fn visit_dcl_comma_list(&mut self, node: &'a DeclarationCommaList);

    fn visit_dcl_init(&mut self, node: &'a DeclarationInit);

    fn visit_dcl(&mut self, node: &'a Declaration);

    fn visit_assign_clause(&mut self, node: &'a AssignmentClause);

    fn visit_assign_stmt(&mut self, node: &'a AssignmentStmt);

    fn visit_id_with_assign_clause(&mut self, node: &'a IdWithAssigmentClause);

    fn visit_assign(&mut self, node: &'a Assign);

    fn visit_pre_post_inc_dec_assign(&mut self, node: &'a PrePostIncDecAssign);

    fn visit_assign_inc_dec(&mut self, node: &'a AssignIncDec);

    fn visit_stmt(&mut self, node: &'a Statement);

    fn visit_expr_stmt(&mut self, node: &'a ExpressionStatement);

    fn visit_local_body(&mut self, node: &'a LocalBody);

    fn visit_find_join(&mut self, node: &'a FindJoin);

    fn visit_find_where(&mut self, node: &'a FindWhere);

    fn visit_find_order(&mut self, node: &'a FindOrder);

    fn visit_find_using(&mut self, node: &'a FindUsing);

    fn visit_find_table(&mut self, node: &'a FindTable);

    fn visit_select_table(&mut self, node: &'a SelectTable);

    fn visit_select_stmt(&mut self, node: &'a SelectStmt);

    fn visit_select_opts(&mut self, node: &'a SelectOpts);

    fn visit_table(&mut self, node: &'a Table);

    fn visit_from(&mut self, node: &'a From);

    fn visit_index_hint(&mut self, node: &'a IndexHint);

    fn visit_table_with_fields(&mut self, node: &'a TableWithFields);

    fn visit_join_spec(&mut self, node: &'a JoinSpec);

    fn visit_join_variant(&mut self, node: &'a JoinVariant);

    fn visit_join_clause(&mut self, node: &'a JoinClause);

    fn visit_join_using(&mut self, node: &'a JoinUsing);

    fn visit_join_order(&mut self, node: &'a JoinOrder);

    fn visit_order_group_data(&mut self, node: &'a OrderGroupData);

    fn visit_order_group(&mut self, node: &'a OrderGroup);

    fn visit_group_order_by(&mut self, node: &'a GroupOrderBy);

    fn visit_order_elem(&mut self, node: &'a OrderElem);

    fn visit_direction(&mut self, node: &'a Direction);

    fn visit_first_only(&mut self, node: &'a FirstOnly);

    fn visit_force_placeholder_literal(&mut self, node: &'a ForcePlaceholderLiteral);

    fn visit_function_expr(&mut self, node: &'a FunctionExpression);

    fn visit_column(&mut self, node: &'a Column);

    fn visit_conditional_expr(&mut self, node: &'a expression::ConditionalExpression);

    fn visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData);

    fn visit_expr(&mut self, node: &'a Expression);

    fn visit_comparison_expr(&mut self, node: &'a ComparisonExpression);

    fn visit_as_expr(&mut self, node: &'a AsExpression);

    fn visit_is_expr(&mut self, node: &'a IsExpression);

    fn visit_simple_expr(&mut self, node: &'a SimpleExpression);

    fn visit_simple_expr_data(&mut self, node: &'a SimpleExpressionData);

    fn visit_term(&mut self, node: &'a Term);

    fn visit_term_data(&mut self, node: &'a TermData);

    fn visit_complement_factor(&mut self, node: &'a ComplementFactor);

    fn visit_second_factor(&mut self, node: &'a SecondFactor);

    fn visit_factor(&mut self, node: &'a Factor);

    fn visit_constant(&mut self, node: &'a Constant);

    fn visit_field(&mut self, node: &'a Field);

    fn visit_function(&mut self, node: &'a Function);

    fn visit_parm_elem(&mut self, node: &'a ParmElem);

    fn visit_intrinsics(&mut self, node: &'a Intrinsics);

    fn visit_eval(&mut self, node: &'a Eval);

    fn visit_eval_name(&mut self, node: &'a EvalName);

    fn visit_qualifier(&mut self, node: &'a Qualifier);

    fn visit_if_expr(&mut self, node: &'a expression::IfExpression);

    fn visit_ternary_expr(&mut self, node: &'a expression::TernaryExpression);

    fn visit_logical_op(&mut self, node: &'a LogicalOperator);

    fn visit_comparison_op(&mut self, node: &'a ComparisonOperator);

    fn visit_field_list(&mut self, node: &'a FieldList);
}

pub trait VisitorEnterLeave<'a>: Visitor<'a> {
    fn enter_visit_local_body(&mut self, node: &'a LocalBody) -> bool {
        true
    }

    fn leave_visit_local_body(&mut self, node: &'a LocalBody) -> bool {
        true
    }

    fn enter_visit_dcl_stmt(&mut self, node: &'a DeclarationStmt) -> bool {
        true
    }

    fn leave_visit_dcl_stmt(&mut self, node: &'a DeclarationStmt) -> bool {
        true
    }

    fn enter_visit_dcl_init_list(&mut self, node: &'a DeclarationInitList) -> bool {
        true
    }

    fn leave_visit_dcl_init_list(&mut self, node: &'a DeclarationInitList) -> bool {
        true
    }

    fn enter_visit_dcl_comma_list(&mut self, node: &'a DeclarationCommaList) -> bool {
        true
    }

    fn leave_visit_dcl_comma_list(&mut self, node: &'a DeclarationCommaList) -> bool {
        true
    }

    fn enter_visit_dcl_init(&mut self, node: &'a DeclarationInit) -> bool {
        true
    }

    fn leave_visit_dcl_init(&mut self, node: &'a DeclarationInit) -> bool {
        true
    }

    fn enter_visit_dcl(&mut self, node: &'a Declaration) -> bool {
        true
    }

    fn leave_visit_dcl(&mut self, node: &'a Declaration) -> bool {
        true
    }

    fn enter_visit_assign_clause(&mut self, node: &'a AssignmentClause) -> bool {
        true
    }

    fn leave_visit_assign_clause(&mut self, node: &'a AssignmentClause) -> bool {
        true
    }

    fn enter_visit_assign_stmt(&mut self, node: &'a AssignmentStmt) -> bool {
        true
    }

    fn leave_visit_assign_stmt(&mut self, node: &'a AssignmentStmt) -> bool {
        true
    }

    fn enter_visit_id_with_assign_clause(&mut self, node: &'a IdWithAssigmentClause) -> bool {
        true
    }

    fn leave_visit_id_with_assign_clause(&mut self, node: &'a IdWithAssigmentClause) -> bool {
        true
    }

    fn enter_visit_assign(&mut self, node: &'a Assign) -> bool {
        true
    }

    fn leave_visit_assign(&mut self, node: &'a Assign) -> bool {
        true
    }

    fn enter_visit_pre_post_inc_dec_assign(&mut self, node: &'a PrePostIncDecAssign) -> bool {
        true
    }

    fn leave_visit_pre_post_inc_dec_assign(&mut self, node: &'a PrePostIncDecAssign) -> bool {
        true
    }

    fn enter_visit_assign_inc_dec(&mut self, node: &'a AssignIncDec) -> bool {
        true
    }

    fn leave_visit_assign_inc_dec(&mut self, node: &'a AssignIncDec) -> bool {
        true
    }

    fn enter_visit_stmt(&mut self, node: &'a Statement) -> bool {
        true
    }

    fn leave_visit_stmt(&mut self, node: &'a Statement) -> bool {
        true
    }

    fn enter_visit_expr_stmt(&mut self, node: &'a ExpressionStatement) -> bool {
        true
    }

    fn leave_visit_expr_stmt(&mut self, node: &'a ExpressionStatement) -> bool {
        true
    }

    fn enter_visit_find_join(&mut self, node: &'a FindJoin) -> bool {
        true
    }

    fn leave_visit_find_join(&mut self, node: &'a FindJoin) -> bool {
        true
    }

    fn enter_visit_find_where(&mut self, node: &'a FindWhere) -> bool {
        true
    }
    fn leave_visit_find_where(&mut self, node: &'a FindWhere) -> bool {
        true
    }

    fn enter_visit_find_order(&mut self, node: &'a FindOrder) -> bool {
        true
    }
    fn leave_visit_find_order(&mut self, node: &'a FindOrder) -> bool {
        true
    }

    fn enter_visit_find_using(&mut self, node: &'a FindUsing) -> bool {
        true
    }
    fn leave_visit_find_using(&mut self, node: &'a FindUsing) -> bool {
        true
    }

    fn enter_visit_find_table(&mut self, node: &'a FindTable) -> bool {
        true
    }
    fn leave_visit_find_table(&mut self, node: &'a FindTable) -> bool {
        true
    }

    fn enter_visit_select_table(&mut self, node: &'a SelectTable) -> bool {
        true
    }
    fn leave_visit_select_table(&mut self, node: &'a SelectTable) -> bool {
        true
    }

    fn enter_visit_select_stmt(&mut self, node: &'a SelectStmt) -> bool {
        true
    }
    fn leave_visit_select_stmt(&mut self, node: &'a SelectStmt) -> bool {
        true
    }

    fn enter_visit_select_opts(&mut self, node: &'a SelectOpts) -> bool {
        true
    }
    fn leave_visit_select_opts(&mut self, node: &'a SelectOpts) -> bool {
        true
    }

    fn enter_visit_table(&mut self, node: &'a Table) -> bool {
        true
    }
    fn leave_visit_table(&mut self, node: &'a Table) -> bool {
        true
    }

    fn enter_visit_from(&mut self, node: &'a From) -> bool {
        true
    }
    fn leave_visit_from(&mut self, node: &'a From) -> bool {
        true
    }

    fn enter_visit_index_hint(&mut self, node: &'a IndexHint) -> bool {
        true
    }
    fn leave_visit_index_hint(&mut self, node: &'a IndexHint) -> bool {
        true
    }

    fn enter_visit_table_with_fields(&mut self, node: &'a TableWithFields) -> bool {
        true
    }
    fn leave_visit_table_with_fields(&mut self, node: &'a TableWithFields) -> bool {
        true
    }

    fn enter_visit_join_spec(&mut self, node: &'a JoinSpec) -> bool {
        true
    }
    fn leave_visit_join_spec(&mut self, node: &'a JoinSpec) -> bool {
        true
    }

    fn enter_visit_join_variant(&mut self, node: &'a JoinVariant) -> bool {
        true
    }
    fn leave_visit_join_variant(&mut self, node: &'a JoinVariant) -> bool {
        true
    }

    fn enter_visit_join_clause(&mut self, node: &'a JoinClause) -> bool {
        true
    }
    fn leave_visit_join_clause(&mut self, node: &'a JoinClause) -> bool {
        true
    }

    fn enter_visit_join_using(&mut self, node: &'a JoinUsing) -> bool {
        true
    }
    fn leave_visit_join_using(&mut self, node: &'a JoinUsing) -> bool {
        true
    }

    fn enter_visit_join_order(&mut self, node: &'a JoinOrder) -> bool {
        true
    }
    fn leave_visit_join_order(&mut self, node: &'a JoinOrder) -> bool {
        true
    }

    fn enter_visit_order_group_data(&mut self, node: &'a OrderGroupData) -> bool {
        true
    }
    fn leave_visit_order_group_data(&mut self, node: &'a OrderGroupData) -> bool {
        true
    }

    fn enter_visit_order_group(&mut self, node: &'a OrderGroup) -> bool {
        true
    }
    fn leave_visit_order_group(&mut self, node: &'a OrderGroup) -> bool {
        true
    }

    fn enter_visit_group_order_by(&mut self, node: &'a GroupOrderBy) -> bool {
        true
    }
    fn leave_visit_group_order_by(&mut self, node: &'a GroupOrderBy) -> bool {
        true
    }

    fn enter_visit_order_elem(&mut self, node: &'a OrderElem) -> bool {
        true
    }
    fn leave_visit_order_elem(&mut self, node: &'a OrderElem) -> bool {
        true
    }

    fn enter_visit_direction(&mut self, node: &'a Direction) -> bool {
        true
    }
    fn leave_visit_direction(&mut self, node: &'a Direction) -> bool {
        true
    }

    fn enter_visit_first_only(&mut self, node: &'a FirstOnly) -> bool {
        true
    }
    fn leave_visit_first_only(&mut self, node: &'a FirstOnly) -> bool {
        true
    }

    fn enter_visit_force_placeholder_literal(&mut self, node: &'a ForcePlaceholderLiteral) -> bool {
        true
    }
    fn leave_visit_force_placeholder_literal(&mut self, node: &'a ForcePlaceholderLiteral) -> bool {
        true
    }

    fn enter_visit_function_expr(&mut self, node: &'a FunctionExpression) -> bool {
        true
    }
    fn leave_visit_function_expr(&mut self, node: &'a FunctionExpression) -> bool {
        true
    }

    fn enter_visit_column(&mut self, node: &'a Column) -> bool {
        true
    }
    fn leave_visit_column(&mut self, node: &'a Column) -> bool {
        true
    }

    fn enter_visit_conditional_expr(&mut self, node: &'a expression::ConditionalExpression) -> bool {
        true
    }
    fn leave_visit_conditional_expr(&mut self, node: &'a expression::ConditionalExpression) -> bool {
        true
    }

    fn enter_visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData) -> bool {
        true
    }
    fn leave_visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData) -> bool {
        true
    }

    fn enter_visit_expr(&mut self, node: &'a Expression) -> bool {
        true
    }
    fn leave_visit_expr(&mut self, node: &'a Expression) -> bool {
        true
    }

    fn enter_visit_comparison_expr(&mut self, node: &'a ComparisonExpression) -> bool {
        true
    }
    fn leave_visit_comparison_expr(&mut self, node: &'a ComparisonExpression) -> bool {
        true
    }

    fn enter_visit_as_expr(&mut self, node: &'a AsExpression) -> bool {
        true
    }
    fn leave_visit_as_expr(&mut self, node: &'a AsExpression) -> bool {
        true
    }

    fn enter_visit_is_expr(&mut self, node: &'a IsExpression) -> bool {
        true
    }
    fn leave_visit_is_expr(&mut self, node: &'a IsExpression) -> bool {
        true
    }

    fn enter_visit_simple_expr(&mut self, node: &'a SimpleExpression) -> bool {
        true
    }
    fn leave_visit_simple_expr(&mut self, node: &'a SimpleExpression) -> bool {
        true
    }

    fn enter_visit_simple_expr_data(&mut self, node: &'a SimpleExpressionData) -> bool {
        true
    }
    fn leave_visit_simple_expr_data(&mut self, node: &'a SimpleExpressionData) -> bool {
        true
    }

    fn enter_visit_term(&mut self, node: &'a Term) -> bool {
        true
    }
    fn leave_visit_term(&mut self, node: &'a Term) -> bool {
        true
    }

    fn enter_visit_term_data(&mut self, node: &'a TermData) -> bool {
        true
    }
    fn leave_visit_term_data(&mut self, node: &'a TermData) -> bool {
        true
    }

    fn enter_visit_complement_factor(&mut self, node: &'a ComplementFactor) -> bool {
        true
    }
    fn leave_visit_complement_factor(&mut self, node: &'a ComplementFactor) -> bool {
        true
    }

    fn enter_visit_second_factor(&mut self, node: &'a SecondFactor) -> bool {
        true
    }
    fn leave_visit_second_factor(&mut self, node: &'a SecondFactor) -> bool {
        true
    }

    fn enter_visit_factor(&mut self, node: &'a Factor) -> bool {
        true
    }
    fn leave_visit_factor(&mut self, node: &'a Factor) -> bool {
        true
    }

    fn enter_visit_constant(&mut self, node: &'a Constant) -> bool {
        true
    }
    fn leave_visit_constant(&mut self, node: &'a Constant) -> bool {
        true
    }

    fn enter_visit_field(&mut self, node: &'a Field) -> bool {
        true
    }
    fn leave_visit_field(&mut self, node: &'a Field) -> bool {
        true
    }

    fn enter_visit_function(&mut self, node: &'a Function) -> bool {
        true
    }
    fn leave_visit_function(&mut self, node: &'a Function) -> bool {
        true
    }

    fn enter_visit_parm_elem(&mut self, node: &'a ParmElem) -> bool {
        true
    }
    fn leave_visit_parm_elem(&mut self, node: &'a ParmElem) -> bool {
        true
    }

    fn enter_visit_intrinsics(&mut self, node: &'a Intrinsics) -> bool {
        true
    }
    fn leave_visit_intrinsics(&mut self, node: &'a Intrinsics) -> bool {
        true
    }

    fn enter_visit_eval(&mut self, node: &'a Eval) -> bool {
        true
    }
    fn leave_visit_eval(&mut self, node: &'a Eval) -> bool {
        true
    }

    fn enter_visit_eval_name(&mut self, node: &'a EvalName) -> bool {
        true
    }
    fn leave_visit_eval_name(&mut self, node: &'a EvalName) -> bool {
        true
    }

    fn enter_visit_qualifier(&mut self, node: &'a Qualifier) -> bool {
        true
    }
    fn leave_visit_qualifier(&mut self, node: &'a Qualifier) -> bool {
        true
    }

    fn enter_visit_if_expr(&mut self, node: &'a expression::IfExpression) -> bool {
        true
    }
    fn leave_visit_if_expr(&mut self, node: &'a expression::IfExpression) -> bool {
        true
    }

    fn enter_visit_ternary_expr(&mut self, node: &'a expression::TernaryExpression) -> bool {
        true
    }
    fn leave_visit_ternary_expr(&mut self, node: &'a expression::TernaryExpression) -> bool {
        true
    }

    fn enter_visit_logical_op(&mut self, node: &'a LogicalOperator) -> bool {
        true
    }
    fn leave_visit_logical_op(&mut self, node: &'a LogicalOperator) -> bool {
        true
    }

    fn enter_visit_comparison_op(&mut self, node: &'a ComparisonOperator) -> bool {
        true
    }
    fn leave_visit_comparison_op(&mut self, node: &'a ComparisonOperator) -> bool {
        true
    }

    fn enter_visit_field_list(&mut self, node: &'a FieldList) -> bool {
        true
    }
    fn leave_visit_field_list(&mut self, node: &'a FieldList) -> bool {
        true
    }
}